package com.itheima.test;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import 
     org.springframework.context.support.ClassPathXmlApplicationContext;

import com.itheima.dao.CustomerMapper;
import com.itheima.po.Customer;
/**
 * DAO测试类
 */
public class DaoTest {

	@Test
	public void findCustomerByIdMapperTest(){	
	    ApplicationContext act = 
	            new ClassPathXmlApplicationContext("applicationContext.xml");
	    CustomerMapper customerMapper = act.getBean(CustomerMapper.class);   
	    Customer customer = customerMapper.findCustomerById(1);
	    System.out.println(customer);
	}

}
