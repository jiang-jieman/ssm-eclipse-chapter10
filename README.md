# ssm-eclipse-chapter10

#### 介绍
spring + mybatis 整合

使用 mybatis数据库
t_customer 表

表结构为


```

--
-- 表的结构 `t_customer`
--

CREATE TABLE IF NOT EXISTS `t_customer` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `jobs` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='mybatis用SSM课程';

--
-- 转存表中的数据 `t_customer`
--

INSERT INTO `t_customer` (`id`, `username`, `jobs`, `phone`) VALUES
(1, 'rose', 'student', '13333533092'),
(2, 'rose', 'student', '13333533092');

```





